# Redis server running inside a docker container with pm2

# Installation
```sh
$ git clone https://gitlab.com/thomasdorn/coinplot-redis.git
$ cd coinplot-redis
$ sh buildAndRun.sh
```
# Administration commands
```sh
Pm2 list processes
$ docker exec -it $(docker ps -a | grep coinplot-redis | awk '{print $1}') pm2 list
Pm2 show logs
$ docker exec -it $(docker ps -a | grep coinplot-redis | awk '{print $1}') pm2 logs
Pm2 enter container
$ docker exec -it $(docker ps -a | grep coinplot-redis | awk '{print $1}') bash
```